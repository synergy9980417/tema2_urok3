import java.io.*;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!");



//
//        Урок 3. Цикл for
//        Цель задания:
//        Продолжение знакомства с циклами, на это раз знакомство с
//        управляющей конструкцией (циклом)  for, формирование навыков работы с циклом, а
//        также формирование понимания различий циклов while и for.
//        Задание:
//        1.
//        Выведите числа от двух в десятой степени до десяти в четвертой степени

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for (double i=Math.pow(2,10); i<Math.pow(10,4);i++) {
    System.out.println(i);
}

//        2.
//        Сохраните миллион файлов. Получилось?

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

//        for (int i2=0; i2<=1000_000; i2++){
//            File file = new File("files/"+String.valueOf(i2));
//            System.out.println("создан файл files/"+i2 + file.createNewFile());
//
//        }

//        Получилось. но лучше не надо.


//                3.
//        Выведите буквы от а до я (подсказка: for (char letter=’а’; letter<=’я’; letter++)
//)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
       for (char letter='a'; letter<='я';letter++){
           System.out.println(letter);
       }

//        4.
//        выведите десять букв в алфавите после буквы й

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
for (int i='й'; i<=('й'+10);i++){
    System.out.println((char)i);
}

//        5.
//        выведите десять
//        букв в алфавите, идущие до буквы ю
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for (int i='ю'; i<=('ю'+10);i++){
            System.out.println((char)i);
        }

//        6.
//        выведите каждую вторую букву алфавита (а, в, д)
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for (int i='а'; i<=('я');i++){

//            System.out.println(String.valueOf(i));
            if (i%2==0) System.out.println((char)i);
        }

//        7.
//        выведите каждую третью букву латинского алфавита
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        for (int i='a'; i<=('z');i++){
//            System.out.println(String.valueOf(i));
            if (i%3==0) System.out.println((char)i);
        }


//        8.
//        выведите греческий алфавит (
//                коды с
//                945
//                по
//                969)
//        через запятую (α, β, ...)
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        for (int i=945; i<=(969);i++){
//            System.out.println(String.valueOf(i));
          if (i==945) System.out.printf("(");
            if (i<969) System.out.print((char)i+",");
            else
            System.out.printf((char)i+")");

        }

//        9.
//        Выведите в файл list.html список :
//<ul>
//<li> 1 elemen
//        t </li>
//<li> 2 element </li>
//...
//<li> 100 element </li>
//</ul>

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 9 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String file1="files/list.html";
        //создадим писателя
        FileWriter writer1=new FileWriter(file1);

        //отчистим содержимое
        PrintWriter writerClear1 = new PrintWriter(file1);
        writerClear1.print("");
        writerClear1.close();

        for (int i=1;i<=100;i++){
          if (i==1) writer1.write("<ul>\n<li>\n"+String.valueOf(i)+" element\n</li>\n");
          else if (i==100) writer1.write("<li>\n"+String.valueOf(i)+" element\n</li>\n</ul>");
          else writer1.write("<li>\n"+String.valueOf(i)+" element\n</li>\n");
        }
        writer1.close();



//                10.
//        Выведите курс валют за 1 число каждого месяца в 2014 году


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 10 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        for (int i = 0; i <= 12; i++){
//            String url="https://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=01/"+(i<10?("0"+String.valueOf(i)):String.valueOf(i)+"/2014&date_req2=01/")+(i<10?("0"+String.valueOf(i)):String.valueOf(i)+"/2014");
//            System.out.println(url);
//            System.out.println(getStr(url));
//        }
//

        System.out.println("к сожалению API для получения в json формате не работает, приходится парсить html");
        String parsString=getStr("http://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.so=1&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01235&UniDbQuery.From=01.01.2014&UniDbQuery.To=01.12.2014");
    //    System.out.println(parsString);

        for (int i=1;i<=12;i++) {
            String month=(i<10?("0"+String.valueOf(i)):String.valueOf(i));
            String monthAll="<td>01." + month + ".2014</td>        <td>1</td>        <td>";
            int Start = parsString.lastIndexOf(monthAll);

            String kurs=parsString.substring((Start+monthAll.length()),(Start+monthAll.length()+7));
            System.out.println(" 01." + month + ".2014 = (" + kurs + ")");

        }
//        11.
//        Выведите курс валют за 1 февраля  каждого года с 1994 до текущего года

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 11 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        String parsString2=getStr("http://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.so=1&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01235&UniDbQuery.From=01.02.1994&UniDbQuery.To=01.02.2024");
     //   System.out.println(parsString2);

        for (int i=2024;i>=1994;i--) {
               String year=String.valueOf(i);

            String monthAll="<td>01.02."+year+"</td>        <td>1</td>        <td>";
            int Start = parsString2.lastIndexOf(monthAll);

            String kurs=parsString2.substring((Start+monthAll.length()),(i>1997)?(Start+monthAll.length()+7):(Start+monthAll.length()+10));
            System.out.println(" 01.02."+year+" = (" + kurs + ")");

        }

        //        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                н
//        аписан скелет кода для вывода
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов
    }






public static String getStr(String urlStr) {
    StringBuilder result = new StringBuilder();
    String line;
    try {
        URL url = new URL(urlStr);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        while ((line = reader.readLine()) != null) result.append(line);
        reader.close();
    } catch (Exception e) {
// ...
        System.out.println(e.getMessage());
    }

    return result.toString();
}

public static void writeToFile(String str, String fileName) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    writer.write(str);
    writer.close();
}

}